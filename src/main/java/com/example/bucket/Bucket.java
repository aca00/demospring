package com.example.bucket;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Bucket {
    @GetMapping("/bucket")
    public  String sayHello() {
        return "Hello from Bucket";
    }
}
