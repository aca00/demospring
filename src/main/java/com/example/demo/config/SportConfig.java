package com.example.demo.config;

import com.example.demo.utils.coaching.SwimCoach;
import com.example.demo.utils.coaching.interfaces.Coach;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SportConfig {
    @Bean
    public Coach swimCoach() {
        return new SwimCoach();
    }
}
