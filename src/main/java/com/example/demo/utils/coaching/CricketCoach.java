package com.example.demo.utils.coaching;

import com.example.demo.utils.coaching.interfaces.Coach;
import org.springframework.stereotype.Component;

@Component
public class CricketCoach implements Coach {
    @Override
    public String getDailyActivity() {
        return "Throw ball for 10 times";
    }
}
