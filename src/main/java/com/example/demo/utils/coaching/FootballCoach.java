package com.example.demo.utils.coaching;

import com.example.demo.utils.coaching.interfaces.Coach;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class FootballCoach implements Coach {
    @Override
    public String getDailyActivity() {
        return "Football: Run around ground 10 times";
    }
}
