package com.example.demo.utils.coaching;

import com.example.demo.utils.coaching.interfaces.Coach;

public class SwimCoach implements Coach {
    @Override
    public String getDailyActivity() {
        return "Swim: Swim 3 times";
    }
}
