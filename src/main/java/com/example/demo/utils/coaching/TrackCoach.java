package com.example.demo.utils.coaching;

import com.example.demo.utils.coaching.interfaces.Coach;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class TrackCoach implements Coach {
    @Override
    public String getDailyActivity() {
        return "Track: Run 10 km.";
    }

    @PostConstruct
    public void construct() {
        System.out.println("Constructed " + this.getClass().getSimpleName());
    }

    @PreDestroy
    public void destroy () {
        System.out.println("Going to be destroyed " + this.getClass().getSimpleName());
    }
}
