package com.example.demo.utils.coaching.controller;

import com.example.demo.utils.coaching.interfaces.Coach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CoachingRestController {
    private Coach coach;
    private final Coach footBallCoach;
    private final Coach swimCoach;

    @Autowired
    public CoachingRestController(@Qualifier("footballCoach") Coach footBallCoach,
                                  @Qualifier("swimCoach") Coach swimCoach) {
        this.footBallCoach = footBallCoach;
        this.swimCoach = swimCoach;
    }

    @Autowired
    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    @GetMapping("/coach")
    public String getActivity() {
        return coach.getDailyActivity();
    }

    @GetMapping("/coach/football")
    public String getFootballActivity() {
        return footBallCoach.getDailyActivity();
    }

    @GetMapping("/coach/swim")
    public String getSwimActivity() {
        return swimCoach.getDailyActivity();
    }
}
