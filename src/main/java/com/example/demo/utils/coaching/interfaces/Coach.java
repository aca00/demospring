package com.example.demo.utils.coaching.interfaces;

public interface Coach {
    String getDailyActivity();
}
